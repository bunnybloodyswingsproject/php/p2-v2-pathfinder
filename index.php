<!DOCTYPE html>
<html>
<head>
	<title>Breadth-first search Pathfinder</title>
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<link rel="stylesheet" type="text/css" href="./assets/css/style.css">

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css2?family=Press+Start+2P&display=swap" rel="stylesheet">
</head>
<body onload="setup()">
	<div class="this_container">
		<div class="left">
			<div class="maze-container" id="maze-container"></div>
		</div>
		<div class="right">
			<h1>Breadth-first Search <br>Pathfinder</h1>
			<div class="grp_btn">
				<div>
					<button class="buttons solve" onclick="solveMaze()">Solve</button>
					<button class="buttons reset" onclick="reset()">Reset</button>
				</div>
				<div>
					<button class="buttons genBtn" onclick="generate()">Generate Walls</button>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript" src="./assets/js/script.js"></script>
</body>
</html>